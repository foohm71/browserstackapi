# browserstackAPI

## What is this?
API to access Browserstack to run simple Selenium Test

How to run the docker container:
```
docker run -e CMD_EXECUTOR=<command executor> -it -p 5000:33 foohm71/browserstackapi:main
```

The command executor is obtained after you create a BrowserStack account. It looks something like:
`https://foohm71_uPyakF:mkZLYt9vwqW1G99PxUYc@hub-cloud.brserstack.com/wd/hub` 

## How to call the BrowserStack API
The `test.sh` shows you how to make the call to the API. It takes in a payload `test.json`
test.sh looks like this:
```
curl -X POST "http://localhost:5000/test" -d @test.json --header "Content-Type: application/json"
```

`test.json` looks like this:
```
{"url": "http://www.google.com"}
```

## What's with the other files
This docker image was built using a Python script - `main.py`. It uses the BrowserStack API to access the BrowserStack browser farm and Flask to build the API. This is then packaged into a docker image as described in `Dockerfile`. To see how the image is built see `.gitlab-ci.yml`.