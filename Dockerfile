FROM python:3.7

WORKDIR /usr/src/app

COPY requirements.txt /usr/src/app/
RUN pip install --no-cache-dir -r requirements.txt

COPY . /usr/src/app

# Inform Docker that the container is listening on the specified port at runtime.
EXPOSE 5000

# Run command
CMD ["python", "main.py"]

