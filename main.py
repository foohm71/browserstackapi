
#########################
# Selenium part

from selenium import webdriver
import os

# This is only for local testing
#from dotenv import load_dotenv
#load_dotenv()

CMD_EXECUTOR = os.getenv("CMD_EXECUTOR")

#########################
# Browser Stack version

from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

caps=[{
      'os_version': '10',
      'os': 'Windows',
      'browser': 'chrome',
      'browser_version': 'latest',
      'name': 'Parallel Test1', # test name
      'build': 'browserstack-build-1' # Your tests will be organized within this build
      },
      {
      'os_version': '10',
      'os': 'Windows',
      'browser': 'firefox',
      'browser_version': 'latest',
      'name': 'Parallel Test2',
      'build': 'browserstack-build-1'
      },
      {
      'os_version': 'Big Sur',
      'os': 'OS X',
      'browser': 'safari',
      'browser_version': 'latest',
      'name': 'Parallel Test3',
      'build': 'browserstack-build-1'
}]

def runTestBrowserStack(url):
    driver = webdriver.Remote(
      command_executor=CMD_EXECUTOR,
      desired_capabilities=caps[0])
    driver.get(url)
    title = driver.title
    app.logger.info("title = %s", str(title))
    return title

from flask import Flask, request, Response, jsonify
import json

#########################
# Flask part 

app = Flask(__name__)
app.debug = True

@app.route('/test', methods=["GET", "POST"])
def test():
    if request.method == "GET":
        payload = { "error:": "Only POST supported" }
        return jsonify(payload), 404
    elif request.method == "POST":
        data = request.get_json()
        app.logger.info("%s was obtained", str(data))
        
        url = data['url']
        app.logger.info("url = %s", str(url))
        app.logger.info("CMD_EXECUTOR = %s", CMD_EXECUTOR)
        
        title = runTestBrowserStack(url)

        payload = { "title": title }

        return jsonify(payload), 200

# use this to test on localhost:5000
#app.run()

# use this to run in a docker container
app.run(host='0.0.0.0', port='33')
